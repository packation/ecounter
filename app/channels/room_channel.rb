class RoomChannel < ApplicationCable::Channel
  def subscribed
     #message = Message.find(params[:id])
     #stream_for message
     stream_from "room_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
  	  #ActionCable.server.broadcast 'room_channel', message: data['message']
	message = Message.find(1)
	message.increment!(:count)
	ActionCable.server.broadcast 'room_channel', message: message.count
  end
  def reset(data)
	message = Message.find(1)
        message.update!(:count=>0)
        ActionCable.server.broadcast 'room_channel', message: message.count
  end
end
